# THIS IS A WORK IN PROGRESS

# Test files (docs test files)
test_ops() {
  echo 'functions/git/ops.sh ok'
}

# Fetch and rebase
# `docs fr [repo]`
fr() {
  if [[ $1 ]] ; then
      go_$1
      if [[ $? != 0 ]]; then
        invalid_cmd
        echo "Choose a valid repo to fetch and rebase: `tput bold`$ docs fr [repo]`tput sgr0`"
        echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
        return
      else
        DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
        echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
        git_status
        tput setaf 5 ; echo "Fetching $1's $DEF_BRANCH..." ; tput sgr0
        echo `tput bold`"$ git fetch origin $DEF_BRANCH"`tput sgr0`
        git fetch origin $DEF_BRANCH
        tput setaf 5 ; echo "Rebasing $1 against $DEF_BRANCH..." ; tput sgr0
        echo `tput bold` "$ git rebase origin/$DEF_BRANCH" `tput sgr0`
        git rebase origin/$DEF_BRANCH
      fi
  else
    invalid_cmd
    echo "Choose a repo to fetch and rebase: `tput bold`$ docs fr [repo]`tput sgr0`"
    echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
  fi
}

# Fetch and pull
# `docs fp [repo] [repo] [repo]`
fp(){
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      go_$arg
      if [[ $? != 0 ]]; then
        invalid_cmd
        echo "Choose one or more valid repos to fetch and pull: `tput bold`$ docs fp [repo] [repo] [repo]`tput sgr0`"
        echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
        return
      else
        DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
        echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
        git_status
        git_master
        tput setaf 5 ; echo "Fetching $arg's origin..." ; tput sgr0
        echo `tput bold`"$ git fetch origin"`tput sgr0`
        git fetch origin
        tput setaf 5 ; echo "Pulling $arg's $DEF_BRANCH..." ; tput sgr0
        echo `tput bold` "$ git pull --ff-only origin $DEF_BRANCH" `tput sgr0`
        git pull --ff-only origin $DEF_BRANCH
      fi
    done
  else
    invalid_cmd
    echo "Choose one or more repos to fetch and pull: `tput bold`$ docs fp [repo] [repo] [repo]`tput sgr0`"
    echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
  fi
}

# Interactive rebase
# `docs ri [repo] [number of commits]`
ri() {
  if [[ $1 ]] ; then
    go_$1
    if [[ $? != 0 ]]; then
      invalid_cmd
      echo "Choose a valid repo to rebase -i: `tput bold`$ docs ri [repo]`tput sgr0`"
      echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
      echo "Valid command: `tput bold`$ docs ri [repo] [number of commits]`tput sgr0`"
      return
    fi
    if [[ $2 ]]; then
      COMMITS=$2
      git_status
      tput setaf 5 ; echo "Interactively rebasing $COMMITS commits..." ; tput sgr0
      echo `tput bold`"$ git rebase -i HEAD~$COMMITS"`tput sgr0`
      git rebase -i HEAD~$COMMITS
    else
      invalid_cmd
      echo "Choose number of commits to rebase: `tput bold`$ docs ri [repo] [number of commits]`tput sgr0`"
      return
    fi
  else
    invalid_cmd
    echo "Choose a valid repo to rebase interactively and the respective number of commits."
    echo "Valid [repo]s: gitlab, runner, omnibus, charts, docs, shell, gdk"
    echo "Valid command: `tput bold`$ docs ri [repo] [number of commits]`tput sgr0`"
    echo "Example: `tput bold`$ docs ri gitlab 3`tput sgr0`"
  fi
}

