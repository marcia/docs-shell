# Test files (docs test files)
test_lint_content() {
  echo 'functions/lint/content.sh ok'
}

# ---------------------------------------------------------------- #

# Lint GitLab, Omnibus, Runner, Charts

# markdownlint

# Generate .tool-versions file from GitLab Docs' dependencies
# Used a different name so that it does not conflict with possible asdf implementation
asdf_gen_tool_versions(){
  export ASDF_DEFAULT_TOOL_VERSIONS_FILENAME='.docs-tool-versions'
  cd $DOC
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  cd $DSHELL
  curl -s https://gitlab.com/gitlab-org/gitlab-docs/-/raw/$DEF_BRANCH/.tool-versions > .docs-tool-versions
  asdf install>/dev/null
}

# Remove .docs-tool-versions file
asdf_remove_tool_versions(){
  if [[ -e .docs-tool-versions ]]; then
    rm .docs-tool-versions
  fi
}

run_markdownlint(){
  echo `tput setaf 5`"Running markdownlint..." `tput sgr0`
  echo `tput bold`"$ markdownlint --config .markdownlint.yml 'doc/**/*.md'" `tput sgr0`; markdownlint --config .markdownlint.yml 'doc/**/*.md'
}

lint_markdown_gitlab() {
  go_gitlab
  run_markdownlint
  ifsuccess
  cd $DSHELL
}

lint_markdown_omnibus() {
  go_omnibus
  asdf_gen_tool_versions
  run_markdownlint
  ifsuccess
  asdf_remove_tool_versions
  cd $DSHELL
}

lint_markdown_runner() {
  go_runner
  asdf_gen_tool_versions
  echo `tput setaf 5`"Running markdownlint..." `tput sgr0`
  echo `tput bold`"$ markdownlint --config .markdownlint.json 'docs/**/*.md'" `tput sgr0`; markdownlint --config .markdownlint.json 'docs/**/*.md'
  ifsuccess
  asdf_remove_tool_versions
  cd $DSHELL
}

lint_markdown_charts() {
  go_charts
  asdf_gen_tool_versions
  run_markdownlint
  ifsuccess
  asdf_remove_tool_versions
  cd $DSHELL
}

# ---------------------------------------------------------------- #

# Vale

run_vale() {
  echo `tput setaf 5`"Running Vale lint..." `tput sgr0`
  echo `tput bold`"$ vale --minAlertLevel error --glob='*.{md}' doc" `tput sgr0`; vale --minAlertLevel error --glob='*.{md}' doc
}

lint_vale_gitlab() {
  go_gitlab
  run_vale
  ifsuccess
  cd $DSHELL
}

lint_vale_omnibus() {
  go_omnibus
  run_vale
  ifsuccess
  cd $DSHELL
}

lint_vale_runner() {
  go_runner
  echo `tput setaf 5`"Running Vale lint..." `tput sgr0`
  echo `tput bold`"$ vale --minAlertLevel error --glob='*.{md}' docs" `tput sgr0`; vale --minAlertLevel error --glob='*.{md}' docs
  ifsuccess
  cd $DSHELL
}

lint_vale_charts() {
  go_charts
  run_vale
  ifsuccess
  cd $DSHELL
}

# ---------------------------------------------------------------- #

# Lint app/views for broken links and anchors

lint_views_gitlab() {
  go_gitlab
  echo `tput setaf 5`"Running App/Views haml_lint..." `tput sgr0`
  echo `tput bold`"$ haml-lint -i DocumentationLinks" `tput sgr0`; haml-lint -i DocumentationLinks
  ifsuccess
  cd $DSHELL
}

lint_views_gitlab_gdk(){
  cd $GDK_SEP_GL
  echo "I'm in $PWD"
  echo `tput setaf 5`"Running App/Views haml_lint..." `tput sgr0`
  echo `tput bold`"$ haml-lint -i DocumentationLinks" `tput sgr0`; haml-lint -i DocumentationLinks
  ifsuccess
  cd $DSHELL
}

# ---------------------------------------------------------------- #

# All content

lint_all_content() {
  echo `tput setaf 5`"Linting markdownlint in all repos..." `tput sgr0`
  lint_markdown_gitlab
  lint_markdown_omnibus
  lint_markdown_runner
  lint_markdown_charts
  echo `tput setaf 5`"Linting Vale in all repos..." `tput sgr0`
  lint_vale_gitlab
  lint_vale_omnibus
  lint_vale_runner
  lint_vale_charts
  echo `tput setaf 5`"Linting App/Views GitLab..." `tput sgr0`
  lint_views_gitlab
}
