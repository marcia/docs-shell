# GitLab Docs Shell

A command line interface to work locally on [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs).

**IMPORTANT NOTES:** 

- THIS APP IS **NO LONGER ACTIVELY MAINTAINED**. IT IS NOT RECOMMENDED FOR THE USE WITH GITLAB DOCUMENTATION.
- IT IS LIKELY TO BE REPLACED WITH ANOTHER CLI APPLICATION NOT RELATED TO GITLAB DOCS.

## What it does

Introduces an easier and faster way to work on GitLab Documentation through
`docs [options]` commands.

With `docs` commands, you can:

- Set up **everything** to build, preview, and test GitLab Docs locally ([`docs install`](docs/installation.md)).
- Clone all repos (GitLab Docs, GitLab, Omnibus, Runner, and Charts) or set up the local paths to your repos (during `docs install`).
- Check, install, and update [GitLab Docs dependencies](docs/workflows.md#check-dependencies) (during `docs install`, or later, with `docs dep`).
- Pull all repos in one command (`docs pull`) and housekeep to keep them clean (`docs housekeep`).
- Compile GitLab Docs and live preview (during `docs install`, or later, with `docs compile live`). <!-- To-do: preview on mobile. -->
- Run all [linters](docs/workflows.md#linting) (`docs lint`).
- Set up [SSH authentication to GitLab.com](docs/ssh.md) (when repos are cloned via `docs install`, or later, with `docs ssh-gitlab`).
- [Update GDK](docs/gdk.md) (`docs gdk-update`).

See [usage](#usage) for more options.

See [demos](docs/demos.md) for installation and usage video demonstrations.

## Who's this for

- New contributors to GitLab Docs - the easiest way to set it up to work locally.
- Current contributors - the easiest way to update, compile, recompile, and lint GitLab Docs, and to keep the dependencies up-to-date.

## How to use this project

[Install](docs/installation.md) Docs Shell to optimize your workflow for
updating, compiling, linting, and previewing GitLab Docs locally.

## Requirements

Docs Shell is optimized for [iTerm2 + ZSH + oh-my-zsh](https://medium.com/ayuth/iterm2-zsh-oh-my-zsh-the-most-power-full-of-terminal-on-macos-bdb2823fb04c) on macOS Catalina and Big Sur. Its behavior on Linux is unknown.

We assume you're familiar with using Git through the command line and GitLab.

[**ZSH**](#zsh) shell is a **hard-requirement** for Docs Shell. It comes installed on macOS Catalina and Big Sur.

### ZSH

Docs Shell requires ZSH shell to run its scripts. It will be checked during
the installation process, but you can check yourself beforehand if you'd like:

1. To check which shell you're using, open your terminal and run: `echo $SHELL`.
1. If it returns `/bin/zsh`, proceed to [installation](#installation). If not, you can check if you have it installed with `zsh --version`.
1. If it returns a version number, such as `zsh 5.7.1`, set ZSH as default shell: `chsh -s /bin/zsh`.

If ZSH is not present in your computer, see [how to install](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH) it according to your OS.

**Note:** if you'd like Docs Shell to install ZSH, iTerm2, and oh-my-zsh, run `docs pre-install` before running `docs install`, so that you can benefit from their features during the installation process. You can also run it at any moment later.

### GitLab Docs dependencies

During the installation process, **all GitLab Docs' dependencies will be installed** (if not present). Mind that:

- NodeJS: Docs Shell will prompt you to install it with Homebrew.
  - Alternative method: [Install Node with a version manager (nvm)](https://github.com/nvm-sh/nvm#installation-and-update).
  - Alternative method: [Install Node directly](https://nodejs.org/en/download/).
- Ruby: Docs Shell will prompt you to install Ruby with [RBENV](https://github.com/rbenv/rbenv) or [RVM](http://rvm.io/). If you don't use or don't want to use `rvm` to manage Rubies, make sure to install the current [Ruby version](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/master/.ruby-version) of GitLab Docs.
- GitLab Docs' `asdf` dependencies will be checked and installed.
- GitLab's dependencies for content linters will be also checked and installed.

## Installation

See [install Docs Shell](docs/installation.md).

## Update Docs Shell

See [update Docs Shell](docs/update.md).

## Usage

- Check the most common [workflows](docs/workflows.md).
- See [all commands available](docs/commands-reference.md).

## Limitations

- Docs Shell is written and tested on masOS Catalina and Big Sur. If you have a different OS, please test it and open an issue or a merge request in this project if you find any problems.
- Previewing docs partially (for example, compile the site with only `gitlab` docs, not including Omnibus, Runner, and Charts) has not been tested, so it can cause unforeseen errors.
- Docs Shell doesn't support cloning and installing GDK yet, but if you have it already installed in your computer, you can set it up to run [GDK commands with Docs Shell](docs/gdk.md).
- Known issue: a macOS update to Big Sur may break GDK for the concurrent use of `asdf` with RVM or RBENV. In this case, see [GDK troubleshooting](https://gitlab.com/gitlab-org/docs-shell/-/blob/master/docs/gdk.md#gdk-asdfrvm-error).

## Info

For references of commands, shell scripts, and other related infomation, see [info](docs/info.md).

## Contributing

[Contributions](CONTRIBUTING.md) to this project are very welcome.

## License

Copyright (c) 2020 [Marcia Ramos](https://gitlab.com/marcia). See [LICENSE](LICENSE) for
further details.
