# Set up Docs Shell to work with GDK

During the installation process, you'll be prompted to set up [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/).
If you choose not doing so, you can do this later by running `docs gdk-set`.

<!-- to-do: explain gdk on install vs different gdk repo -->

By setting up GDK, you'll tell Docs Shell the path to your GDK repo and will
set the GDK preview port to 3000 while the preview port for GitLab Docs will
become 5000.

Once set, you can do a few things in GDK with Docs Shell:

- To navigate to GDK, run `docs go_gdk`.
- To update GDK (seemlessly), run `docs gdk-update`.
  To start GDK right after updating, run `docs gdk-update --start`.
- To lint FE in GitLab's GDK (`eslint`, `jest`, and `prettier`), run `docs gdk-lint-fe`.
- To lint and write prettier suggestions, run `docs gdk-lint-fe -w`.

## Troubleshooting GDK

If you got an error updating GDK:

- Try running `docs gdk-update-tbs` on your terminal open in GDK.
- If the previous command didn't work, try `docs gdk-update-tbs --deep`.

### GDK asdf/rvm error

If your `gdk update` failed and `gdk doctor` outputted this error:

```shell
RVM and asdf
--------------------------------------------------------------------
RVM and asdf appear to both be enabled. This has been known to cause
issues with asdf compiling tools.
```

It means that you have conflicts between rvm and asdf. To fix this, try the following process:

1. Uninstall your Ruby versions installed through rvm. For example, to uninstall Ruby 3.0.0, run: `rvm remove 3.0.0`.
1. Uninstall your Ruby versions installed through adsf. For example: `asdf uninstall ruby 3.0.0`.
<!-- 1. Read the logs and install any remaining gems. This will install them through asdf's Ruby. -->
1. Run `docs asdf-config-legacy` to set [asdf legacy file to true](https://asdf-vm.com/#/core-configuration?id=homeasdfrc).
1. Run `docs dep` to reinstall all the dependencies.
1. Run `docs gdk-update` or `docs gdk-update --start` (to start GDK after running `docs gdk-update`).
