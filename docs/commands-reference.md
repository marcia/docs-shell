# Commands reference

A list of `docs [option]` commands available.

## Pre-installation

```bash
# Grant terminal access to Docs Shell (required):
cd docs-shell
. ./docs.sh && access

# Test or trigger `docs`:
docs hello

# SSH GitLab:
docs ssh-gitlab

# Pre-install dependencies:
docs pre-install # option to check and install ZSH, iTerm2, oh-my-zsh
```

## Installation

```bash
# Test or trigger `docs`:
docs hello

# Docs Shell usage:
docs --help

# Installation (required):
docs install
```

## Reinstallation

```bash
# Reinstall Docs Shell (necessary to update to version 1.0.5 or higher)
docs reinstall

# Reinstall and clone all repos
docs reinstall --clone

# Reinstall from a different docs-shell directory
cd docs-shell # open the terminal in the new repo
. ./docs.sh && reinstall # set the current docs-shell directory and reinstall Docs Shell
. ./docs.sh && reinstall --clone # set the current docs-shell directory, clone all repos, and reinstall Docs Shell
```

## Maintenance

```bash
# Change paths:
docs update-paths # Change paths to existing repos

# Check/install latest dependencies:
docs dep

# Update system's dependendencies:
docs dep --update

# Update Docs Shell - pulls from upstream master and pushes to origin master:
docs shell --update

# Housekeep all repos:
# Runs `git gc --prune=30.minutes.ago`.
# There's a risk to execute this operation.
# For details, see https://git-scm.com/docs/git-gc
docs housekeep
```

## Linting

```bash
# Lint:
docs lint # lints both content and links
# - Lint options:
docs lint --content | -c # lint content
docs lint --nanoc   | -n # lint gitlab-docs

# Lint content options:
docs lint -c [repo]
# - Example:
docs lint -c gitlab
# - Lint content repo options:
docs lint -c [repo] [linter]
# - Examples:
docs lint -c gitlab markdown
docs lint -c gitlab vale
# - Lint GitLab UI app/views links
docs lint -c gitlab views

# Lint Nanoc options:
docs lint -n
# Options:
docs lint -n anchors # lint for broken anchors
docs lint -n links # lint for broken internal links
docs lint -n elinks # lint for broken external links

# gitlab-docs development linters
docs lint --dev | -d # lint dev linters
docs lint -d [linter] # lint specific linter
# Linters: yarn, jest, scss, yaml, rspecs, ci
# Examples:
docs lint -d yaml
docs lint -d scss
```

## Compiling

```bash
# Compile:
docs compile
docs compile live # Compile and live preview
docs compile live lint # Compile, live preview, lint

# Preview GitLab Docs:
docs live

# Recompile:
docs recompile
docs recompile lint # recompile and lint content and nanoc
docs recompile lint live # recompile, lint content and nanoc, preview docs
```

## Working on content

```bash
# Pull repos
docs pull # checkout the default branch, pull all repos
docs pull [repo] [repo] ... [repo] # checkout the default branch, pull [repo]
# - Examples:
docs pull gitlab # pull GitLab
docs pull gitlab runner # Pull GitLab and GitLab Runner
docs pull docs # Pull GitLab Docs
```

### GDK

Note: this is a work in progress, you may face unforeseen errors.
Cloning GDK is not available yet.

```bash
# Set up GDK
docs gdk-set
# Update GDK
docs gdk-update
# Pass the flag `--min` for minimal update
docs gdk-update --min

# FE test in GitLab's GDK (eslint, jest, and prettier)
docs gdk-lint-fe
# Pass the flag `-w` for prettier --write
docs gdk-lint-fe -w
```

## Troubleshooting

### Reset repos

Runs `git checkout <default-branch>` and `git reset --hard origin/<default-branch>`.

```bash
# Hard-reset [repo]
docs reset [repo]

# Examples:
docs reset gitlab # resets your GitLab repo
docs reset runner # resets your Runner repo
```

## Nanoc

```bash
# Nanoc direct commands
docs nanoc compile
docs nanoc live

# Kill nanoc live
docs kill
```

## Navigation

```bash
# Navigation
docs go_gitlab
docs go_docs
docs go_runner
docs go_omnibus
docs go_charts
docs go_shell
docs go_gdk
```
