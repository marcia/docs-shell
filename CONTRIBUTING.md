# Contribute to Docs Shell

Docs Shell is in early development and it needs your help. Your feedback is
very welcome.

If you know how to clean up shell scripts, or have other suggestions to
improve the code, please do contribute to Docs Shell.

If you can, please submit a merge request to this project, it will be sorely
appreaciated. If you want to report any bugs, request new functionalities or
improvements, please feel free to submit an issue.

If you want to contribute code to this project:

- Use this project extensively to understand the logic of its functions.
- Watch for the consistency of the code in a file and try to keep it consistent.
- Use the [Google Shell Style Guide](https://google.github.io/styleguide/shellguide.html) as reference.
- See [GitLab's shell scripting standards](https://docs.gitlab.com/ee/development/shell_scripting_guide/) for reference.
- Test your code before submitting your merge request. Make sure that it doens't break other functions.
- Document your changes. Follow GitLab's [documentation style guidelines](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html) as reference and use [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html) markup.

Please bear in mind that this is a side project and isn't among GitLab's
higher priorities.

Currently Docs Shell is developed and maintained by @marcia to help onboarding
contributors to GitLab Docs.
