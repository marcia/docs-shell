# Script for ZSH

# Test file (docs test files)
test_install_ruby() {
  echo 'install/ruby.sh ok'
}

# ---------------------------------------------------------------- #

# Load current gitlab-docs Ruby (from the default branch)
# This function loads as soon as a `docs [option]` command runs
check_current_docs_ruby() {
  cd $DSHELL
  gldocs_ruby
  LOCALRUBY=$(localrv)
  export LOCALRUBY="$LOCALRUBY"
  if [[ $LOCALRUBY != $DOCSRUBY ]] ; then
    echo `tput setaf 1`"Your local Ruby version does not match GitLab Docs'."`tput sgr0`
    error_sound ; tput setaf 3 ; read "REPLY?Recommended: Would you like to install Ruby version $DOCSRUBY now (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      install_ruby
    else
      echo "Ok."
    fi
  fi
}

# ---------------------------------------------------------------- #

# Check gitlab-docs and system Rubies

# Update $DOCSRUBY
gldocs_ruby() {
  # Check if GitLab.com is up
  if ping -c 1 -q gitlab.com >&/dev/null; then
    cd $DOC
    DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
    cd $DSHELL
    GLDOCSRUBY=$(curl -s https://gitlab.com/gitlab-org/gitlab-docs/-/raw/$DEF_BRANCH/.ruby-version > .docs-ruby && cat .docs-ruby)
    if (grep 'DOCSRUBY' functions/assets/temp-variables.sh >/dev/null) ; then
      sed -i '' '/DOCSRUBY/d' functions/assets/temp-variables.sh
    fi
    echo "DOCSRUBY='$GLDOCSRUBY'" >> functions/assets/temp-variables.sh
    export DOCSRUBY="$DOCSRUBY"
    source functions/assets/temp-variables.sh
    rm .docs-ruby
  else
    status_code=$(curl -s -o /dev/null -I -w %{http_code} https://gitlab.com/)
    echo "GitLab.com is not reachable. HTTP status: $status_code"
  fi
}

# Cat Ruby version
docsrv() {
  gldocs_ruby
}

# Get local Ruby version
localrv() {
  cd $DSHELL
  # get 2nd word | get first 1-5 chars
  ruby --version | cut -d' ' -f2 | cut -c 1-5
}

# ---------------------------------------------------------------- #

# Check RVM

# Install RVM
rvm-install() {
  echo `tput bold` "$ curl -sSL https://get.rvm.io | bash -s stable" `tput sgr0` ; curl -sSL https://get.rvm.io | bash -s stable
}

# Install gitlab-docs Ruby with RVM
rvm-install-ruby() {
  # Get updated Ruby version
  docsrv
  echo `tput bold` "$ rvm install $DOCSRUBY" `tput sgr0` ; rvm install $DOCSRUBY
  echo `tput bold` "$ rvm use $DOCSRUBY" `tput sgr0` ; rvm use $DOCSRUBY
  # Ignore rvm warning for Gemfile's Ruby
  rvm rvmrc warning ignore allGemfiles
}

# rvm all set (check/install rvm, rvm install Ruby)
rvm-set() {
  if ! depcheck_rvm ; then
    rvm-install
  fi
  if ! depcheck_ruby ; then
    rvm-install-ruby
  fi
  tput setaf 2; printf "Rubies ready! `tput sgr0` (${NOW}) \n"
}

depcheck_rvm() {
  echo `tput setaf 5`"Checking RVM...." `tput sgr0`
  echo `tput bold` '$ rvm --version' `tput sgr0` ; rvm --version
}

check_rvm() {
  if ! depcheck_rvm ; then
    tput setaf 3
    read  "REPLY?Would you like to install RVM (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      rvm-set
      set_rvmanager
    else
      echo "Okay, let's try to proceed without a Ruby Version Manager."
    fi
  fi
} 

# ---------------------------------------------------------------- #

# Check rbenv

# Check if rbenv exists and output the current Ruby version
depcheck_rbenv() {
  echo `tput setaf 5`"Checking RBENV..." `tput sgr0`
  echo `tput bold` '$ rbenv version' `tput sgr0` ; rbenv version
}

# If rbenv isn't installed, give option to install RBENV or to use RVM
check_rbenv() {
  if ! depcheck_rbenv ; then
    if ! depcheck_rvm ; then
      tput setaf 3
      read  "REPLY?Would you like to install RBENV (y/n)? (Type 'n' to use RVM instead)"
      tput sgr0
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        echo `tput bold` '$ brew install rbenv' `tput sgr0` ; brew install rbenv
        RBENV_PATH=$(echo 'export PATH="$HOME/.rbenv/bin:$PATH"' ; echo "")
        echo "$RBENV_PATH" >> ~/.zshrc
        RBENV_INIT=$(echo 'eval "$(rbenv init -)"'; echo "")
        echo "$RBENV_INIT" >> ~/.zshrc
        source ~/.zshrc
        set_rvmanager
        echo "Checking rbenv installation with rbenv-doctor..."
        curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
        ifsuccess
        rbenv_install_ruby
      else
        check_rvm
      fi
    fi
  fi
}

# Install rbenv

# Install Docs Ruby version with rbenv, set shell to use that version
rbenv_install_ruby(){
  docsrv
  echo `tput bold` "$ rbenv install $DOCSRUBY" `tput sgr0` ; rbenv install $DOCSRUBY
  rbenv shell $DOCSRUBY >/dev/null
}

# ---------------------------------------------------------------- #

# Define which Ruby version manager

# Add RVMANAGER to variables
set_rvmanager() {
  if (rbenv version) ; then
    if (grep 'RVMANAGER' functions/assets/temp-variables.sh >/dev/null) ; then
      sed -i '' '/RVMANAGER/d' functions/assets/temp-variables.sh
    fi
    echo "RVMANAGER='rbenv'" >> functions/assets/temp-variables.sh
    export RVMANAGER="$RVMANAGER"
    source functions/assets/temp-variables.sh
    tput setaf 2; echo "Your Ruby version manager is RBENV." ; tput sgr0
  elif (rvm --version) ; then
    if (grep 'RVMANAGER' functions/assets/temp-variables.sh >/dev/null) ; then
      sed -i '' '/RVMANAGER/d' functions/assets/temp-variables.sh
    fi
    echo "RVMANAGER='rvm'" >> functions/assets/temp-variables.sh
    export RVMANAGER="$RVMANAGER"
    source functions/assets/temp-variables.sh
    tput setaf 2; echo "Your Ruby version manager is RVM." ; tput sgr0
  else
    if (grep 'RVMANAGER' functions/assets/temp-variables.sh >/dev/null) ; then
      sed -i '' '/RVMANAGER/d' functions/assets/temp-variables.sh
    fi
    unset RVMANAGER
    echo "Consider installing RBENV or RVM as a Ruby version manager."
  fi
  source functions/assets/temp-variables.sh
}

# ---------------------------------------------------------------- #

# Check Ruby

# Check if Ruby is installed and use gitlab-docs Ruby
depcheck_ruby() {
  echo `tput setaf 5`"Checking GitLab Docs' Ruby version..."`tput sgr0`
  docsrv
  if [[ $RVMANAGER == 'rvm' ]] ; then
    echo `tput bold`"$ rvm use $DOCSRUBY"`tput sgr0` ; rvm use $DOCSRUBY
  else [[ $RVMANAGER == 'rbenv' ]]
    rbenv shell $DOCSRUBY
  fi
  echo `tput bold`'$ ruby --version'`tput sgr0` ; ruby --version
}

check_ruby() {
  if ! depcheck_ruby ; then
    error_sound ; tput setaf 3 ; read "REPLY?Required: you need Ruby $DOCSRUBY. Would you like to install Ruby through a Ruby version manager (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      check_rbenv
    else
      echo `tput setaf 1`"Failed, you need Ruby $DOCSRUBY." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference.\nAborting.'
      false
    fi
  else
    echo `tput setaf 5`"Checking if your local Ruby version meets the requirement..." `tput sgr0`
    check_compare_rubies
  fi
}

check_compare_rubies() {
  gldocs_ruby
  RVM=$DOCSRUBY
  export LOCRUBY=$(localrv)
  echo "GitLab Docs (remote origin) Ruby version is `tput bold`$DOCSRUBY`tput sgr0`."
  echo "Local Ruby version is `tput bold`$LOCRUBY`tput sgr0`."

  if [[ $LOCRUBY == $RVM ]] ; then
    echo `tput setaf 2`'Rubies match.'`tput sgr0`
  else
    echo `tput setaf 1`'Failed, Rubies do not match.'`tput sgr0`
    error_sound ; tput setaf 3 ; read "REPLY?Recommended: Would you like to install Ruby version $DOCSRUBY (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      install_ruby
    else
      error_sound ; echo `tput setaf 1`"Failed, you need Ruby $DOCSRUBY." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference. \n Aborting.'
      false
    fi
  fi
}

# Install Docs Ruby with either rvm or rbenv
install_ruby() {
  if [[ $RVMANAGER == 'rvm' ]] ; then
    rvm-install-ruby
  elif [[ $RVMANAGER == 'rbenv' ]]; then
    rbenv_install_ruby
  else
    check_rbenv
  fi
}
