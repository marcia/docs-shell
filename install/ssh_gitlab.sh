# Test files (`docs test files`)

test_ssh() {
  echo 'install/ssh_gitlab.sh ok'
}

# ---------------------------------------------------------------- #

# SSH GitLab

ssh-gitlab(){
  if ! ssh_check; then
    tput setaf 1; echo "SSH authentication to GitLab failed."; tput sgr0
    renew-ssh
  fi
}

ssh_ask(){
  ok_sound
  read "REPLY?`tput setaf 3`Do you want to add an existing SSH key pair (y/n)? `tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ssh_user_key
    else
      generate_ssh_key
    fi
}

# Check SSH
ssh_check(){
  ssh -T git@gitlab.com
}

# Add SSH key
add_ssh(){
  ssh-add $SSH_KEY
}

# Create/update user's SSH key
ssh_key_env_var(){
  # If there's already fixed variables files
  if [ -e functions/assets/fixed-variables.sh ] ; then
    # Delete reference to SSH key
    if (grep 'SSH_KEY' functions/assets/fixed-variables.sh >/dev/null) ; then
      sed -i '' '/SSH_KEY/d' functions/assets/fixed-variables.sh
    fi
  fi
  # Add new key
  echo "SSH_KEY='$SSH_KEY'" >> functions/assets/fixed-variables.sh
  export SSH_KEY="$SSH_KEY"
  source functions/assets/fixed-variables.sh
}

# Generate new key
generate_ssh_key() {
  unset USER_EMAIL
  unset GLMAIL

  tput setaf 5
  echo "Generating an SSH key..."
  tput sgr0
  ok_sound
  tput setaf 3
  echo "Enter the email associated with your GitLab account:"
  tput sgr0
  read USER_EMAIL
  export GLMAIL=$USER_EMAIL

  tput setaf 2
  echo "Next, enter a password for your SSH key."
  tput sgr0
  echo "Remember this password, you will need it later and it will not be stored by Docs Shell."
  tput sgr0
  
  # Unset SSH_KEY and date
  unset SSH_KEY
  unset DATE
  # Set date
  DATE=$(date +"%Y-%m-%d_%Hh%Mmin%Ss")

  # Set and export key name
  export SSH_KEY_NAME="docs_shell_gitlab_$DATE"
  # Check user home directory (UPATH)
  cd ~
  UPATH=$(pwd)
  cd $DSHELL
  # Set and export key path
  export SSH_KEY="$UPATH/.ssh/$SSH_KEY_NAME"

  # Generate new key
  tput setaf 5
  echo "Generating SSH key pair with 'ed25519' encryption..."
  tput sgr0
  ssh-keygen -t ed25519 -f $SSH_KEY -C "$GLMAIL"

  if [[ -e $SSH_KEY ]]; then
    echo "Your new key was generated. You can find it at $SSH_KEY."
    echo `tput setaf 5`"Copying your SSH public key to clipboard..." `tput sgr0`
    tr -d '\n' < $USER_KEY.pub | pbcopy

    echo "Your public key has been copied to clipboard. To add it to your GitLab account,"
    echo "paste into the 'Key' field on https://gitlab.com/-/profile/keys."
    open https://gitlab.com/-/profile/keys

    read "REPLY?`tput setaf 3`When done, press 'c' to continue.`tput sgr0`"
    if [[ $REPLY =~ ^[Cc]$ ]] ; then
      # Add SSH identity
      add_ssh
      # Created/update $SSH_KEY
      ssh_key_env_var
      tput setaf 2 ; ssh_check ; tput sgr0
      ssh_origin_docs_shell
    fi
  else
    error_sound
    tput setaf 1 ; echo "Failed. Please try again." ; tput sgr0
    echo "If you still have problems, see how to generate an SSH key manually: https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair."
  fi
}

# Renew SSH session
renew-ssh(){
  if [ -e functions/assets/fixed-variables.sh ] ; then
    if (grep 'USER_KEY' functions/assets/fixed-variables.sh >/dev/null) ; then
        # Add key
        source functions/assets/fixed-variables.sh
        export USER_KEY="$USER_KEY"
        echo `tput setaf 5`"Adding SSH identity..."`tput sgr0`
        echo `tput bold`"$ ssh-add $USER_KEY"`tput sgr0`
        ssh-add $USER_KEY
        # Check connection
        echo `tput setaf 5`"Checking connection..."`tput sgr0`
        echo `tput bold`"$ ssh -T git@gitlab.com"`tput sgr0`
        ssh -T git@gitlab.com
      elif (grep 'SSH_KEY' functions/assets/fixed-variables.sh >/dev/null) ; then
        # Unset old variable
        unset SSH_KEY
        # Source variable
        source functions/assets/fixed-variables.sh
        # Export SSH_KEY to run with ssh-add
        export SSH_KEY="$SSH_KEY"
        # Add new key
        ssh-add $SSH_KEY
        # Check connection
        ssh -T git@gitlab.com
    else
      echo "There's no SSH key set with Docs Shell."
      ok_sound
      read "REPLY?`tput setaf 3`Set up SSH now (y/n)?`tput sgr0`"
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          ssh_ask
        else
          echo "Ok. Aborting."
        fi
    fi
  else
    echo "There's no SSH key set with Docs Shell."
    ok_sound
    read "REPLY?`tput setaf 3`Set up SSH now (y/n)?`tput sgr0`"
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        ssh_ask
      else
        echo "Ok. Aborting."
      fi
  fi
}

# Generate a new key pair without checking SSH connection
new-ssh-key(){
  generate_ssh_key
}

# Check if Docs Shell was cloned via HTTPS and replace origin with SSH
ssh_origin_docs_shell(){
  if (git remote -v | grep -i -r --col "https://gitlab.com/gitlab-org/docs-shell.git" >/dev/null ); then
    git remote -v
    echo `tput setaf 5`"Setting remote origin to git@gitlab.com:gitlab-org/docs-shell.git..."`tput sgr0`
    echo `tput bold`'$ git remote remove origin'`tput sgr0`
    git remote remove origin
    echo `tput bold`'$ git remote add origin git@gitlab.com:gitlab-org/docs-shell.git'`tput sgr0`
    git remote add origin git@gitlab.com:gitlab-org/docs-shell.git
  fi
}

# Add current user's key pair
ssh_user_key(){
  echo "`tput setaf 3`Enter the path to your key (without .pub extension):`tput sgr0`"
  read USER_KEY_INPUT
  USER_KEY=$USER_KEY_INPUT
  # Delete reference to SSH key
  if (grep 'USER_KEY' functions/assets/fixed-variables.sh >/dev/null) ; then
    sed -i '' '/USER_KEY/d' functions/assets/fixed-variables.sh
    echo "Current key was reset."
  fi
  # Set new key
  echo "USER_KEY='$USER_KEY'" >> functions/assets/fixed-variables.sh
  source functions/assets/fixed-variables.sh
  export USER_KEY="$USER_KEY"
  # Add new key
  echo `tput setaf 5`"Adding new SSH identity..."`tput sgr0`
  echo `tput bold`"$ ssh-add $USER_KEY"`tput sgr0`
  ssh-add $USER_KEY
  # Check connection
  echo `tput setaf 5`"Checking connection..."`tput sgr0`
  echo `tput bold`"$ ssh -T git@gitlab.com"`tput sgr0`
  ssh -T git@gitlab.com
  ssh_origin_docs_shell
}
