# Test files (docs test files)
test_paths() {
  echo 'paths.sh ok'
}

# ---------------------------------------------------------------- #

# Read the folder this repo was cloned into and set it as $LOCAL
# To check, run echo $LOCAL

_loc(){
  cd .. && pwd
}

export CURRENT=$PWD

# # ---------------------------------------------------------------- #

# Custom paths for clone-temp
# Temporary solution for GL team members until complete refactor of the clone/paths functions

paths(){
  # Define all repos dir
  export LOCAL=$(_loc)
  export GL="$LOCAL/"

  # Remove default paths if they exist
  if [ -e functions/assets/default-paths.sh ] ; then
    rm functions/assets/default-paths.sh
  fi

  # Remove custom paths if they exist
  if [ -e functions/assets/custom-paths.sh ] ; then
    rm functions/assets/custom-paths.sh
  fi

  # Remove .paths (older version, deprecated) if they exist
  if [ -e .paths ] ; then
    rm .paths
  fi

  # Define new local paths
  echo "LOCAL='$LOCAL'" > functions/assets/custom-paths.sh
  echo "GL='$GL'" >> functions/assets/custom-paths.sh
  echo "DSHELL='$LOCAL/docs-shell'" >> functions/assets/custom-paths.sh
  export DSHELL="$DSHELL"
  source functions/assets/custom-paths.sh

  # GitLab Shell
  tput setaf 3
  ok_sound ; read "REPLY?Is `tput sgr0`$LOCAL/docs-shell`tput setaf 3` the path to your Docs Shell repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    if (grep 'DSHELL' functions/assets/custom-paths.sh >/dev/null) ; then
      sed -i '' '/DSHELL/d' functions/assets/custom-paths.sh
    fi
    DSHELL=$GL'docs-shell'
    echo "DSHELL='$DSHELL'" >> functions/assets/custom-paths.sh
    export DSHELL="$DSHELL"
  else
    echo "Enter the path to your Docs Shell repo:"
    read CUSTOM_DSHELL
    DSHELL=$CUSTOM_DSHELL
    if (grep 'DSHELL' functions/assets/custom-paths.sh >/dev/null) ; then
      sed -i '' '/DSHELL/d' functions/assets/custom-paths.sh
    fi
    echo "DSHELL='$CUSTOM_DSHELL'" >> functions/assets/custom-paths.sh
    export DSHELL="$DSHELL"
  fi

  # GitLab Docs
  tput setaf 3
  ok_sound ; read "REPLY?Is `tput sgr0`$LOCAL/gitlab-docs`tput setaf 3` the path to your GitLab Docs repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    export DOC=$GL'gitlab-docs'
    echo "DOC='$DOC'" >> functions/assets/custom-paths.sh
  else
    echo "Enter the path to your GitLab Docs repo:"
    read CUSTOM_DOC
    export DOC=$CUSTOM_DOC
    echo "DOC='$CUSTOM_DOC'" >> functions/assets/custom-paths.sh
  fi

  # GitLab
  ok_sound ; tput setaf 3 ; read "REPLY?Is `tput sgr0`$LOCAL/gitlab`tput setaf 3` the path to your GitLab repo (y/n)?" ; tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    export GITLAB=$GL'gitlab'
    echo "GITLAB='$GITLAB'" >> functions/assets/custom-paths.sh
    export DGDK='1'
    # Set GitLab Docs port to 3000
    echo "PORT='3000'" >> $DSHELL/functions/assets/user-variables.sh
    tput setaf 2 ; echo "Preview GitLab Docs on port 3000." ; tput sgr0
    source $DSHELL/functions/assets/user-variables.sh
    symlink_gitlab
  else
    ok_sound ; tput setaf 3 ; read "REPLY?Is it within GDK (y/n)?" ; tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      gdk_set_path
      export GITLAB="$GDK_SEP/gitlab"
      echo "GITLAB='$GDK_SEP/gitlab'" >> $DSHELL/functions/assets/custom-paths.sh
      source $DSHELL/functions/assets/custom-paths.sh
      symlink_gdk
    else
      ok_sound ; echo "Enter the path to your GitLab repo:"
      read CUSTOM_GITLAB
      export GITLAB=$CUSTOM_GITLAB
      echo "GITLAB='$CUSTOM_GITLAB'" >> functions/assets/custom-paths.sh
      export DGDK='1'
      # Set GitLab Docs port to 3000
      echo "PORT='3000'" >> $DSHELL/functions/assets/user-variables.sh
      tput setaf 2 ; echo "Preview GitLab Docs on port 3000." ; tput sgr0
      source $DSHELL/functions/assets/user-variables.sh
      symlink_gitlab
    fi
  fi

  # Omnibus
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/omnibus-gitlab`tput setaf 3` the path to your Omnibus GitLab repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    export OMN=$GL'omnibus-gitlab'
    echo "OMN='$OMN'" >> functions/assets/custom-paths.sh
    symlink_omnibus
  else
    ok_sound ; echo "Enter the path to your Omnibus GitLab repo:"
    read CUSTOM_OMN
    export OMN=$CUSTOM_OMN
    echo "OMN='$CUSTOM_OMN'" >> functions/assets/custom-paths.sh
    symlink_omnibus
  fi

  # Runner
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/gitlab-runner`tput setaf 3` the path to your GitLab Runner repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    export RUN=$GL'gitlab-runner'
    echo "RUN='$RUN'" >> functions/assets/custom-paths.sh
    symlink_runner
  else
    ok_sound ; echo "Enter the path to your GitLab Runner repo:"
    read CUSTOM_RUN
    export RUN=$CUSTOM_RUN
    echo "RUN='$CUSTOM_RUN'" >> functions/assets/custom-paths.sh
    symlink_runner
  fi

  # Charts
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/charts`tput setaf 3` the path to your GitLab Charts repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    export CHAR=$GL'charts'
    echo "CHAR='$CHAR'" >> functions/assets/custom-paths.sh
    symlink_charts
  else
    ok_sound ; echo "Enter the path to your GitLab Charts repo:"
    read CUSTOM_CHAR
    export CHAR=$CUSTOM_CHAR
    echo "CHAR='$CUSTOM_CHAR'" >> functions/assets/custom-paths.sh
    symlink_charts
  fi
  source $DSHELL/functions/assets/custom-paths.sh
}

# `docs update-paths`
update-paths(){
  paths
}

echo_paths(){
  if [ -e functions/assets/user-variables.sh ] ; then
    source functions/assets/user-variables.sh
  fi
  if [ -e functions/assets/temp-variables.sh ] ; then
    source functions/assets/temp-variables.sh
  fi
  if [ -e functions/assets/default-paths.sh ] ; then
    source functions/assets/default-paths.sh
  fi
  if [ -e functions/assets/custom-paths.sh ] ; then
    source functions/assets/custom-paths.sh
  fi
  echo "The path to Docs Shell is set to `tput bold`$DSHELL`tput sgr0`."
  echo "The path to GitLab Docs is set to `tput bold`$DOC`tput sgr0`."
  echo "The path to GitLab is set to `tput bold`$GITLAB`tput sgr0`."
  echo "The path to Runner is set to `tput bold`$RUN`tput sgr0`."
  echo "The path to Omnibus is set to `tput bold`$OMN`tput sgr0`."
  echo "The path to Charts is set to `tput bold`$CHAR`tput sgr0`."
  if [[ $GDK_SEP ]]; then
    echo "The path to GDK is set to `tput bold`$GDK_SEP`tput sgr0`."
  fi
}
